package comspringtoolsappproyecto.model.dao;






import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;

import comspringtoolsappproyecto.model.entity.Ventas;

public interface IVentas extends JpaRepository<Ventas, Long>{
	@Query("select count(*) from Ventas")
	public int contarVentas();

	@Query("select sum(v.total) from Ventas v where v.fecha BETWEEN ?1 AND ?2 ")
	public  Float calcularSuma(@Param("desde")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)Date des,@Param("hasta")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)Date has);
	
	

}
